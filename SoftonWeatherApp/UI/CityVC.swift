//
//  CityVC.swift
//  SoftonWeatherApp
//
//  Created by Brian Rojas on 10/7/18.
//  Copyright © 2018 briantarget. All rights reserved.
//

import UIKit

class CityVC: UIViewController {

    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var cloudImage: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    
    @IBOutlet weak var tempMinLabel: UILabel!
    @IBOutlet weak var tempMedLabel: UILabel!
    @IBOutlet weak var tempMaxLabel: UILabel!
    
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var atmLabel: UILabel!
    @IBOutlet weak var winSpeedLabel: UILabel!
    
    var cityItem: CityDTO? {
        didSet {
            // Update the view.
            configureView()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    
    func configureView() {
        // Update the user interface for the detail item.
        if let city = cityItem {
            self.navigationItem.title = city.name;
            
            if let label = countryLabel {
                label.text = city.country
            }
            if let imageView = cloudImage {
                if city.imageIcon != "" {
                    imageView.image = UIImage(named: city.imageIcon)
                }
            }
            if let label2 = temperatureLabel {
                label2.text = String(lround(city.temperature))
            }
            if let label3 = weatherDescriptionLabel {
                label3.text = city.description
            }
            if let label4 = tempMinLabel {
                label4.text = String(lround(city.tempMin))
            }
            if let label5 = tempMedLabel {
                let tempMed = city.tempMin + ((city.tempMax - city.tempMin)/2)
                label5.text = String(lround(tempMed))
            }
            if let label6 = tempMaxLabel {
                label6.text = String(lround(city.tempMax))
            }
            if let label7 = humidityLabel {
                label7.text = String(city.humidity) + "%"
            }
            if let label8 = atmLabel {
                label8.text = "0"
            }
            if let label9 = winSpeedLabel {
                let windKMHour = city.windSpeed * 3.6
                label9.text = String(lround(windKMHour))
            }
            
        }
    }

}


//
//  CityTableViewCell.swift
//  SoftonWeatherApp
//
//  Created by Brian Rojas on 10/9/18.
//  Copyright © 2018 briantarget. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var cloudImage: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   /* override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }*/
    
}

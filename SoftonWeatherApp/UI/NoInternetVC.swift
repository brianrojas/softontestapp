//
//  NoInternetVC.swift
//  SoftonWeatherApp
//
//  Created by Brian Rojas on 10/9/18.
//  Copyright © 2018 briantarget. All rights reserved.
//

import UIKit

class NoInternetVC: UIViewController {
    
    weak var delegate : ParentCitiesListProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func retryAction(_ sender: Any) {
        retryInternet()
    }
    
}

//MARK: - Network Methods
extension NoInternetVC {
    
    
    func retryInternet() -> Void {
        
        if SWNetwotkWrapper.isConnectedToInternet {
            print("Yes! internet is available.")
            // do some tasks..
            delegate?.retryWithInternet()
            dismiss(animated: true, completion: nil)
        }else{
            print("no internet connection try again later.")
            
            return
        }
    }
}

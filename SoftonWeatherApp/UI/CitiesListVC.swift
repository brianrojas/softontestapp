//
//  CitiesListVC.swift
//  SoftonWeatherApp
//
//  Created by Brian Rojas on 10/7/18.
//  Copyright © 2018 briantarget. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

protocol ParentCitiesListProtocol : class
{
    func retryWithInternet()
}

class CitiesListVC: UITableViewController {

    var detailViewController: CityVC? = nil
    var cityObjects = [CityDTO]()
    var lat = 9.748917
    var lon = -83.753426
    var locationManager:CLLocationManager!
    
    private let refreshController = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Navigation Code
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? CityVC
        }
        
        self.tableView.register(UINib.init(nibName: "CityTableViewCell", bundle: nil), forCellReuseIdentifier: "CityCell")
        
        //Heredia
        //loadCities(lat: 9.998910, lon: -84.116478)
        // Costa Rica
        //loadCities(lat: 9.748917, lon: -83.753426)
        // New York
        //loadCities(lat: 40.712776, lon: -74.005974)
        
        initiateLocationManager()
        
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshController
        } else {
            tableView.addSubview(refreshController)
        }
        // Configure Refresh Control
        refreshController.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
        
        refreshController.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
        
        determineMyCurrentLocation()
        
    }

    
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCity" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = cityObjects[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! CityVC
                controller.cityItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
        
        if segue.identifier == "showCheckNetwork" {
            let controller = segue.destination as! NoInternetVC
            controller.delegate = self
        }
        
    }
    
    func showModal() {
        self.performSegue(withIdentifier: "showCheckNetwork", sender: self)
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Alert", message: "Location Services are not Enabled, Please enable Location services to get your Weather!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        
        loadCities(lat: lat, lon: lon)
        
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityObjects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCell", for: indexPath) as! CityTableViewCell

        let city = cityObjects[indexPath.row]
        cell.cityLabel?.text = city.name
        cell.temperatureLabel?.text = String(lround(city.temperature))
        if city.imageIcon != "" {
            cell.cloudImage?.image = UIImage(named: city.imageIcon)
        }
        //cell.textLabel!.text = object.name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showCity", sender: indexPath)
    }
    
    

    

}

//MARK: - Network Methods
extension CitiesListVC {
    
    
    func loadCities(lat: Double, lon: Double) -> Void {
        
        if SWNetwotkWrapper.isConnectedToInternet {
            print("Yes! internet is available.")
            // do some tasks..
        }else{
            print("no internet connection try again later.")
            showModal()
            return
        }
        
        // Heredia
        //http://api.openweathermap.org/data/2.5/find?lat=9.998910&lon=-84.116478&cnt=10&appid=831489fa7ffbbf448cdc5cbc741f9f98
        
        var citiesArray:[CityDTO] = []
        
        let strURL = "http://api.openweathermap.org/data/2.5/find?"
        var param: [String:Any] = [:]
        param["lat"] = lat
        param["lon"] = lon
        param["cnt"] = 25
        param["units"] = "metric"
        param["appid"] = "831489fa7ffbbf448cdc5cbc741f9f98"
        
        SWNetwotkWrapper.requestGETURL(strURL,param: param, success: {
            (JSONResponse) -> Void in
            if JSONResponse["cod"].intValue == 200 {
                print("Response Success!")
                let cityList = JSONResponse["list"].arrayValue
                for city in cityList {
                    //print(city["name"].stringValue)
                    let cityDTO = self.passCityToCityDTO(city: city)
                    citiesArray.append(cityDTO)
                    DispatchQueue.main.async {
                        self.cityObjects = citiesArray
                        self.tableView .reloadData()
                        self.refreshController.endRefreshing()
                    }
                }
            }
        }) {
            (error) -> Void in
            print(error)
            DispatchQueue.main.async {
                self.refreshController.endRefreshing()
            }
        }
    }
    
}

//MARK: - Translation Methods
extension CitiesListVC {
    
    func passCityToCityDTO(city:JSON) -> CityDTO {
        
        let name = city["name"].stringValue
        var temp = 0.0
        var humidity = 0
        var tempMin = 0.0
        var tempMax = 0.0
        let windSpeed : Double
        var rain = 0.0
        let clouds : Int
        var weather : [WeatherDTO] = []
        
        let mainData = city["main"].dictionaryValue
        for dictionary in mainData {
            //print(dictionary.key)
            if dictionary.key == "temp" {
                temp = dictionary.value.doubleValue
            }
            else if dictionary.key == "humidity" {
                humidity = dictionary.value.intValue
            }
            else if dictionary.key == "temp_min" {
                tempMin = dictionary.value.doubleValue
            }
            else if dictionary.key == "temp_max" {
                tempMax = dictionary.value.doubleValue
            }
        }
        let wind = city["wind"].dictionaryValue
        let speedIndex = wind.index(forKey: "speed")
        windSpeed = wind[speedIndex!].value.doubleValue
        
        let rainArray = city["rain"].arrayValue
        if rainArray.count > 0 {
            rain = rainArray[0].doubleValue
        }
        
        let cloudsDictionary = city["clouds"].dictionaryValue
        let cloudsAllIndex = cloudsDictionary.index(forKey: "all")
        clouds = cloudsDictionary[cloudsAllIndex!].value.intValue
        
        let weatherArray = city["weather"].arrayValue
        for weatherItem in weatherArray {
            let weatherItemData = passWeatherToWeatherDTO(weather: weatherItem)
            weather.append(weatherItemData)
        }
        
        let cityDTO = CityDTO(name: name, country: "",
                              temperature: temp, humidity: humidity,
                              tempMin: tempMin, tempMax: tempMax, windSpeed: windSpeed,
                              rain: rain, clouds: clouds, weather: weather)
        return cityDTO
    }
    
    
    func passWeatherToWeatherDTO(weather:JSON) -> WeatherDTO {
        
        let weatherName = weather["main"].stringValue
        let weatherDescription = weather["description"].stringValue
        let weatherIcon = weather["icon"].stringValue
        
        let weatherItemData = WeatherDTO(name: weatherName, description: weatherDescription,icon: weatherIcon)
        return weatherItemData
    }
    
}

//MARK: - Location Methods
extension CitiesListVC: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        //stop listening for location updates
        manager.stopUpdatingLocation()
        
        print("latitude : \(userLocation.coordinate.latitude)")
        print("longitude : \(userLocation.coordinate.longitude)")
        
        lat = userLocation.coordinate.latitude
        lon = userLocation.coordinate.longitude
        
        //loadCities from WebService then populate the tableview
        loadCities(lat: lat, lon: lon)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    func initiateLocationManager(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    func determineMyCurrentLocation() {
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                //print("No access")
                showAlert()
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationManager.startUpdatingLocation()
                //locationManager.startUpdatingHeading()
            }
        }
        else {
            showAlert()
        }
    }
    
}

extension CitiesListVC : ParentCitiesListProtocol {
    func retryWithInternet(){
        //loadCities from WebService then populate the tableview
        loadCities(lat: lat, lon: lon)
    }
}

//
//  SWNetworkWrapper.swift
//  SoftonWeatherApp
//
//  Created by Brian Rojas on 10/8/18.
//  Copyright © 2018 briantarget. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

typealias SuccessBlock = (JSON) -> Void
typealias ErrorBlock = (Error) -> Void

class SWNetwotkWrapper: NSObject {
    //
    class func requestGETURL(_ strURL: String, param: [String:Any], success:@escaping SuccessBlock, failure:@escaping ErrorBlock) {
        Alamofire.request(strURL,parameters: param).responseJSON { (responseObject) -> Void in
            
            //print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

//
//  WeatherDTO.swift
//  SoftonWeatherApp
//
//  Created by Brian Rojas on 10/9/18.
//  Copyright © 2018 briantarget. All rights reserved.
//

import Foundation

struct WeatherDTO {
    
    var name: String
    var description: String
    var icon: String = ""
    
}

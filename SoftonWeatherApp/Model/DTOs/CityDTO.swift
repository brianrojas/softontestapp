//
//  CityDTO.swift
//  SoftonWeatherApp
//
//  Created by Brian Rojas on 10/8/18.
//  Copyright © 2018 briantarget. All rights reserved.
//

import Foundation

struct CityDTO {
    
    var name: String
    var country: String
    
    var temperature: Double
    var humidity: Int
    var tempMin: Double
    var tempMax: Double
    var windSpeed: Double
    var rain: Double
    var clouds: Int
    var weather: [WeatherDTO]
    
    var imageIcon: String {
        get {
            var imageCloud = ""
            
            for weatherItem in weather {
                if weatherItem.icon != "" {
                    if weatherItem.icon == "01d" {
                        //sun
                        imageCloud = "sunny"
                    }
                    else if weatherItem.icon == "02d" {
                        //sun cloud
                        imageCloud = "sun - cloudy"
                    }
                    else if weatherItem.icon == "09d" || weatherItem.icon == "09n"{
                        //rain
                        imageCloud = "rainy"
                    }
                    else if weatherItem.icon == "10d" {
                        //sun rainy
                        imageCloud = "sun - rainy"
                    }
                }
            }
            return imageCloud
        }
    }
    
    var description : String {
        get {
            var descriptionString = ""
            var spaceString = " "
            for weatherItem in weather {
                if weatherItem.description != "" {
                    if descriptionString != "" {
                        spaceString = ", "
                    }
                    descriptionString += spaceString + weatherItem.description
                }
            }
            return descriptionString.capitalized
        }
    }
    
}

